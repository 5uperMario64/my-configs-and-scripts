#!bin/bash

selectWallpaper() {
  filePath="$(ls /home/bjoern/Pictures/wallpapers/*/*.png | shuf -n 1)";
  mimeType="$(file --mime-type $filePath | awk '{print $2}')";
  currentWallpaper="/home/bjoern/Pictures/wallpapers/currentWallpaper.png";
  
  # i3lock only supports image/png
  if [ "$mimeType" != "image/png" ]
  then
    selectWallpaper;
  else
    screenRes="$(xrandr | grep '*' | awk '{print $1}')";
    convert $filePath -resize $screenRes! $currentWallpaper;
    i3lock -i $currentWallpaper -t;
  fi
}

# Check if there are any pictures first
filePath="$(ls /home/bjoern/Pictures/wallpapers/*/*.png | shuf -n 1)";
if [ -n "$filePath" ]
then
  selectWallpaper;
else
  echo "Error: No files in path!";
fi
